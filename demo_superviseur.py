import matplotlib.pyplot as plt
import socket
import time
from coapthon.client import superviseur_local

from coapthon.client.helperclient import HelperClient
from coapthon.client.superviseur_local import SuperviseurLocal, SuperviseurLocalFiltre
from coapthon.utils import parse_uri


host, port, path = parse_uri("coap://localhost:5683/basic")
try:
    tmp = socket.gethostbyname(host)
    host = tmp
except socket.gaierror:
    pass

print('start client')
client = HelperClient(server=(host, port))
print('client started')
client.protocol.superviseur = SuperviseurLocalFiltre(client)
super = client.protocol.superviseur

rtt_l = []
rtt_s = []

N_rep = 100

for n_rep in range(N_rep):
    # print('rep{}'.format(n_rep))
    response = client.get(path)
    rtt_l.append(super.RTT_L)
    rtt_s.append(super.RTT_S)
    # time.sleep(1)
    # print("{} : \n{}".format(n_rep, response.pretty_print()))
client.stop()

print(super.min_RTT, super.avg_RTT, super.tau_retransmission)

fig, axs = plt.subplots(2)

axs[0].hist(super.RTTs, 100, density=True)

axs[0].set_xlabel('RTT (s)')
axs[0].set_xlim(left=0)

# axs[1].step(range(N_rep), super.RTTs, where='post', label='$RTT$')
axs[1].plot(rtt_s, label="$RTT_S$")
axs[1].plot(rtt_l, label="$RTT_L$")

axs[1].set_ylim(bottom=0)
axs[1].set_xlabel('Nombre de message')
axs[1].set_ylabel('RTT (s)')
axs[1].legend()

fig.suptitle('Resultat sur un serveur local')

fig.tight_layout()
fig.savefig('demo.png')
