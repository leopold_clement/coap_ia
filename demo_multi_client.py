import socket
import threading
import time

import matplotlib.pyplot as plt

from coapthon.client.helperclient import HelperClient
from coapthon.client.superviseur import (SuperviseurLocal,
                                         SuperviseurLocalFiltre)
from coapthon.utils import parse_uri

N_rep = 100
N_client = 200

host, port, path = parse_uri("coap://raspberrypi.local/basic")
try:
    tmp = socket.gethostbyname(host)
    host = tmp
except socket.gaierror:
    pass

clients = [HelperClient(server=(host, port)) for _ in range(N_client)]

supers = []
for client in clients:
    client.protocol.superviseur = SuperviseurLocal(client)
    supers.append(client.protocol.superviseur)


def experience(client, N_rep):
    for n_rep in range(N_rep):
        response = client.get(path)
    client.stop()


threads = [threading.Thread(target=experience, args=[
                            client, N_rep], name='Thread-experience-{}'.format(n)) for n, client in enumerate(clients)]

for thread in threads:
    thread.start()

for thread in threads:
    thread.join()

fig, axs = plt.subplots(3, sharex=True)
for n, ax in enumerate(axs):
    ax.hist(supers[n].RTTs, 100, density=True)

axs[-1].set_xlabel('RTT (s)')
# axs[-1].set_xlim(left=0)

fig.tight_layout()
fig.savefig('demo.png')

for n, super in enumerate(supers):
    print("{:<5} | {:.5E} | {:.5E} | {:3^%} | {:0>5} | {:0>5}".format(n, super.min_RTT,
                                                                      super.avg_RTT, super.taux_retransmission, super._n_envoie, super._n_tokken))
