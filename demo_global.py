import socket
import threading
import time


import matplotlib.pyplot as plt

from coapthon.client.helperclient import HelperClient
from coapthon.client.superviseur import (SuperviseurLocal,
                                         SuperviseurLocalFiltre,
                                         SuperviseurGlobal)
from coapthon.utils import parse_uri

N_rep = 50
N_client = 25

host, port, path = parse_uri("coap://raspberrypi.local/basic")
try:
    tmp = socket.gethostbyname(host)
    host = tmp
except socket.gaierror:
    pass

clients = [HelperClient(server=(host, port)) for _ in range(N_client)]

super_global = SuperviseurGlobal(clients, SuperviseurLocalFiltre)


def experience(client, N_rep):
    for n_rep in range(N_rep):
        response = client.get(path)
    client.stop()


threads = [threading.Thread(target=experience, args=[
                            client, N_rep], name='Thread-experience-{}'.format(n)) for n, client in enumerate(clients)]

for thread in threads:
    thread.start()

for thread in threads:
    thread.join()

print(super_global.state)

#[client.stop() for client in clients]
