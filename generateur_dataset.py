import datetime
import random
import socket
import time

import yaml

from coapthon.client.helperclient import HelperClient
from coapthon.client.superviseur import SuperviseurGlobal, SuperviseurLocal
from coapthon.utils import parse_uri
from utils_learning import RequettePeriodique

n_capteur = 25
n_superviseur = 8

n_tirage_RTO = 8

tempdir = "dataset/"

host, port, path = parse_uri("coap://raspberrypi.local/basic")
try:
    tmp = socket.gethostbyname(host)
    host = tmp
except socket.gaierror:
    pass


def produit_cartesien(l1, l2):
    inter = [[elem1+elem2 for elem1 in l1] for elem2 in l2]
    sum = []
    for elem in inter:
        sum += elem
    return sum


def puissance_cartesienne(l1, n):
    if n <= 1:
        return l1
    return produit_cartesien(puissance_cartesienne(l1, n-1), l1)


def tirage_charge():
    return [random.choice([2, 3, 4, 5, 7, 10, 11, 9]) for _ in range(n_capteur)]


def tirage_RTO():
    return [random.uniform(0.01, 2) for _ in range(n_capteur)]


super_gs = [SuperviseurGlobal([HelperClient(server=(host, port)) for _ in range(
    n_capteur)], SuperviseurLocal) for _ in range(n_superviseur)]
requettes = [[RequettePeriodique(super_gs[idx_super].clients[idx_client], 5, path)
              for idx_client in range(n_capteur)] for idx_super in range(n_superviseur)]

[[requette.start() for requette in line] for line in requettes]

file = open(tempdir+'data.yaml', 'a')
file.write("# run du {}-{}-{}\n".format(datetime.datetime.now().date(),
                                        datetime.datetime.now().hour, datetime.datetime.now().minute))
file.close()

while True:
    etat = tirage_charge()
    print(etat)
    for line in requettes:
        for n, requette in enumerate(line):
            requette.period = etat[n]

    data = []
    for n_iter in range(n_tirage_RTO):
        print(n_iter)
        rto_tests = [tirage_RTO() for _ in range(n_superviseur)]
        for rto,  super_g in zip(rto_tests, super_gs):
            super_g.set_rto(rto)
        for super_g in super_gs:
            super_g.reset()
        time.sleep(30)
        for rto,  super_g in zip(rto_tests, super_gs):
            rtt_local = []
            n_tokkens = []
            n_envoies = []
            n_echec = []

            for client in super_g.clients:
                rtt_local.append(client.superviseur.RTTs)
                n_tokkens.append(client.superviseur._n_token)
                n_envoies.append(client.superviseur._n_envoie)
                n_echec.append(client.superviseur._n_echec)

            data.append({
                'rtos': rto,
                'rtts': rtt_local,
                'n_tokens': n_tokkens,
                'n_envoies': n_envoies,
                'n_echec': n_echec
            })
    file = open(tempdir+'data.yaml', 'a')
    file.write(yaml.dump([{
        'charge': etat,
        'mesures': data
    }]))
    file.close()
