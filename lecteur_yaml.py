import yaml
import numpy as np
from matplotlib import pyplot as plt

file = open('dataset/data.yaml', 'r')
# file = open('dataset/data_restr.yaml', 'r')

data = yaml.safe_load(file)

file.close()

charge_tot_rtt = []
charge_tot_retrans = []
rtts = []
taux_retrans = []

for experience_charge in data:
    charge_local = np.sum(30/np.array(experience_charge['charge'])/25)
    for experience_capt in experience_charge['mesures']:
        for capt in experience_capt['rtts']:
            rtts += capt
            charge_tot_rtt += len(capt) * [charge_local]
        for n_t, n_e in zip(experience_capt['n_tokens'], experience_capt['n_envoies']):
            if n_e != 0:
                taux_retrans.append(1 - n_t/n_e)
                charge_tot_retrans += [charge_local]
plt.figure(dpi=1)
fig, axs = plt.subplots(2, 1, sharex=True, figsize=(6, 6), dpi=1000)
hex0 = axs[0].hexbin(charge_tot_rtt, rtts)  # , gridsize=20)
hex1 = axs[1].hexbin(charge_tot_retrans, taux_retrans)  # , gridsize=20)

axs[0].set_ylabel('$RTT (s)$')
axs[1].set_ylabel("taux de retransmition")

axs[-1].set_xlabel('charge du réseau')

fig.tight_layout()
fig.savefig('charge-rtts.png')
fig.savefig('charge-rtts.svg')
