import socket
import threading
import time

import matplotlib.pyplot as plt
import numpy as np

from coapthon.client.helperclient import HelperClient
from coapthon.client.superviseur import (SuperviseurGlobal, SuperviseurLocal,
                                         SuperviseurLocalFiltre)
from coapthon.utils import parse_uri

host, port, path = parse_uri("coap://raspberrypi.local/basic")
try:
    tmp = socket.gethostbyname(host)
    host = tmp
except socket.gaierror:
    pass


def experience(client, N_rep):
    for n_rep in range(N_rep):
        response = client.get(path)
    client.stop()


N_REQUETTE = 20

results = []

N_clients = np.linspace(1, 150, 100, dtype=np.int)
try:
    for n_client in N_clients:
        print("Test à {}".format(n_client))
        clients = [HelperClient(server=(host, port)) for _ in range(n_client)]
        super_global = SuperviseurGlobal(clients, SuperviseurLocalFiltre)
        threads = [threading.Thread(target=experience, args=[
            client, N_REQUETTE], name='T-{}-{}'.format(n_client, n)) for n, client in enumerate(clients)]
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        results.append(super_global.state)
        time.sleep(3)
except KeyboardInterrupt:
    [thread.join() for thread in threads]
    [client.close() for client in clients]

fig, axs = plt.subplots(3, 1, sharex=True)


for idx in range(3):
    axs[idx].plot(N_clients[0:len(results)], [results[n][idx][0]
                                              for n, _ in enumerate(results)])

axs[0].set_ylabel("""Taux de\nretransmission""")
axs[2].set_ylabel("""$\\frac{min_{rtt}}{avg_{rtt}}$""")
axs[3].set_ylabel("""$\\frac{rtt_s}{rtt_l}$""")

axs[-1].set_xlabel("""nombre de requette simultanées""")

fig.tight_layout()
fig.savefig("""n_client_saturation.png""")
fig.savefig("""n_client_saturation.svg""")
