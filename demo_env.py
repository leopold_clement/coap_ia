import socket
import time

from coapthon.client.helperclient import HelperClient
from coapthon.client.superviseur import (SuperviseurGlobal,
                                         SuperviseurLocalFiltre)
from coapthon.utils import parse_uri
from utils_learning import MaquetteCoapEnv, RequettePeriodique

host, port, path = parse_uri("coap://raspberrypi.local/basic")
try:
    tmp = socket.gethostbyname(host)
    host = tmp
except socket.gaierror:
    pass

clients = [HelperClient(server=(host, port)) for _ in range(5)]

environment = MaquetteCoapEnv(
    clients, SuperviseurLocalFiltre, SuperviseurGlobal, path)

requests = [RequettePeriodique(client, 2, path, name="Spamer {}".format(
    n)) for n, client in enumerate(clients)]
[request.start() for request in requests]
while True:
    print(environment.step(5*[0]))
